import 'package:flutter/material.dart';

void main() =>
  runApp(const MyApp());


class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    var link = 'https://images.ctfassets.net/hrltx12pl8hq/28ECAQiPJZ78hxatLTa7Ts/2f695d869736ae3b0de3e56ceaca3958/free-nature-images.jpg?fit=fill&w=1200&h=630';
    return MaterialApp(
      home: Scaffold(
        body: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Colors.deepPurple,
                Colors.blue
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight
            )
          ),
          child: SafeArea(
            child: SizedBox(
              width: double.infinity,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CircleAvatar(
                    radius: 75,
                    foregroundImage: NetworkImage(link),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 15),
                    child: Text(
                        'Ameer Abo Shar',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 28,
                        fontWeight: FontWeight.w800,
                        fontFamily: 'Pixelify Sans'
                      ),
                    ),
                  ),
                  Text(
                      'Flutter Developer',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.w600,
                        fontFamily: 'Pixelify Sans'
                    ),
                  ),
                  InfoCard(
                      icon: Icons.email_outlined,
                      text: 'ameer.mamoon@gmail.com',
                      color: Colors.red
                  ),
                  InfoCard(
                      icon:Icons.phone_outlined,
                      text:'+963 934 801 138',
                      color: Colors.blue
                  ),
                  InfoCard(
                      icon:Icons.location_on_outlined,
                      text:'Damascus , Syria',
                      color: Colors.indigo
                  ),
                ],
              ),
            ),
          ),
        )
      ),
    );
  }
}


class InfoCard extends StatelessWidget {
  final String text;
  final IconData icon;
  final Color? color;
  const InfoCard({
    required this.text,
    this.color,
    required this.icon,
    Key? key
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 320,
      height: 50,
      margin: const EdgeInsets.all(15),
      padding: EdgeInsets.only(left: 12.5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(7.5),
        color: Colors.white,
      ),
      child: Row(
        children: [
          Icon(
            icon,
            size: 35,
            color: color ?? Colors.black,
          ),
          SizedBox(
            width: 12.5,
          ),
          Text(
            text,
            style: TextStyle(
                fontSize: 18,
                color: color ?? Colors.black
            ),
          )
        ],
      ),
    );
  }
}


